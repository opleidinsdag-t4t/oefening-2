﻿using System;
using System.Collections.Generic;
using System.Text;
using T4T.Opleidingsdag.Producer.Events;

namespace T4T.Opleidingsdag.Consumer
{
    public class ShoppingBasket
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public List<Item> Items { get; set; }
        public bool IsOrdered { get; set; }

        public ShoppingBasket()
        {
            Items = new List<Item>();
        }

        public void ApplyChange(object @event)
        {
            switch (@event)
            {
                case ShoppingBasketCreated shoppingBasketCreated:
                    this.Id = shoppingBasketCreated.ShoppingBasketId;
                    this.UserId = shoppingBasketCreated.UserId;
                    break;
                case ItemAddedToShoppingBasket itemAddedToShoppingBasket:
                    var item = new Item
                    {
                        Id = itemAddedToShoppingBasket.ItemId,
                        Quantity = itemAddedToShoppingBasket.Quantity
                    };

                    this.Items.Add(item);
                    break;
                case OrderedItemsInShoppingBasket orderedItemsInShoppingBasket:
                    this.IsOrdered = true;
                    break;
            }
        }
    }
}
