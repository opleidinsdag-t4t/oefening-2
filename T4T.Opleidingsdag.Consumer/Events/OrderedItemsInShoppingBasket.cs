﻿using System;

namespace T4T.Opleidingsdag.Producer.Events
{
    public class OrderedItemsInShoppingBasket
    {
        public Guid ShoppingBasketId { get; set; }
    }
}
