﻿using System;

namespace T4T.Opleidingsdag.Producer.Events
{
    public class ItemAddedToShoppingBasket
    {
        public Guid ShoppingBasketId { get; set; }
        public Guid ItemId { get; set; }
        public int Quantity { get; set; }
    }
}
