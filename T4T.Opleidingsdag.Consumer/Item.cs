﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T4T.Opleidingsdag.Consumer
{
    public class Item
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
    }
}
