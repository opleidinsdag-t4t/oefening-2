﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Streams;

namespace T4T.Opleidingsdag.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var shoppingBasket = new ShoppingBasket();

            using (var store = new MsSqlStreamStore(new MsSqlStreamStoreSettings("Server=.;Database=T4T.Opleidingsdag.Oefening1;Trusted_Connection=True;")))
            {
                var streamId = new StreamId("Foo");

                var page = store.ReadStreamForwards(streamId, 0, 10).Result;

                foreach (var message in page.Messages)
                {
                    var jsonData = message.GetJsonData().Result;
                    var @event = JsonConvert.DeserializeObject(jsonData, Type.GetType(message.Type));

                    shoppingBasket.ApplyChange(@event);
                }
            }

            Console.WriteLine($"Shoppingbasket: {shoppingBasket.Id}, User: {shoppingBasket.UserId}, Num Items: {shoppingBasket.Items.Count}, IsOrdered: {shoppingBasket.IsOrdered}");
            Console.ReadKey();
        }
    }
}
